# Zombie Defense

Ce projet a été créé durant l'été 2018. Celui-ci était principalement pour moi un moyen d'apprendre un maximum de fonctionnalités sur Unity. Ce jeu est intentionnellement simple, je ne souhaitais pas créer un jeu complet mais un jeu simple et fonctionnel.

C'est un jeu en 2D de défense dans lequel vous devrez résister à des vagues de zombies. Vous aurez à votre disposition 3 types d'armes (pistolet, sniper, pistolet-mitrailleur).

Voici quelques images :
![Menu Principal](https://i.imgur.com/mJhVUJW.png "menu principal")
![En jeu 1](https://i.imgur.com/jcNhsEs.png "en jeu 1")
![En jeu 2](https://i.imgur.com/7fMKSGw.png "en jeu 2")

Sur l'interface, vous aurez la liste des armes dont vous disposer ainsi que celle qui est équippé, la barre d'endurance, ainsi que le nombre de munitions dans le chargeur et le nombre de chargeurs. Vous aurez également en haut de l'écran le numéro de vague et le nombre d'enemies restant dans la vague en cours.

Au cours des différentes vagues, des caisses d'armes ou de munition vont apparaître. Si vous disposez déjà de l'arme, cela vous donnera des munitions pour le type d'arme en question, sinon vous obtiendrez l'arme ainsi qu'un certain nombre de munitions pour l'arme.

Chaque arme dispose d'un temps de rechargement et/ou d'un temps d'arrêt entre chaque tir. Vous aurez un indicateur visuel de ces deux facteurs lorsqu'ils entrent en jeu.

_Voici les touches :_
- Se déplacer avec ZQSD.
- Tirer avec clic gauche.
- Viser avec clic droit (nécessaire afin de pouvoir tirer avec le sniper).
- Changer d'arme avec la molette (vous devez avoir débloqué l'arme au préalable dans une caisse).
- Sprinter avec MAJ (Attention : Si la barre se vide, la regénération de l'endurance sera plus lente).
- Attaquer au corps à corps avec F.
- Accéder au menu de pause avec ÉCHAP. 

Ce projet a été développé en environ un mois et malgré le fait qu'un certain nombre de bugs ont été réglés, il subsiste cependant encore quelques bugs dont je ne connais pas l'existance ou dont je ne connais pas la cause. Je suis conscient que certains problèmes peuvent apparaîtrent (notamment lors des animations par exemple) et j'ai essayé d'en régler un maximum mais ce jeu étant ma première expérience en projet Unity solo, je n'ai pas pris le temps de tout régler pour pouvoir me concentrer sur d'autres projets.

En ce qui concerne l'évolution du jeu, je n'ai pas encore décidé si j'allais continuer d'ajouter du contenu ou non. Cependant, j'ai pensé à un certain nombre d'ajouts tels que :
- Une évolution des sprites existants et ajout de nouveaux sprites.
- Ajout de nouveaux effets (plus de sang ejecté lors d'un coup réussi par exemple, de façon aléatoire dans un cône derrière le zombie ou encore un effet de muzzleflash pour les armes).
- Une évolution des animations actuelles.
- Plus de types d'enemies pour plus de variété dans les vagues.
- Plus d'armes (shotgun notamment).
- Une évolution du terrain actuel (un peu vide pour le moment) et de nouveaux types de terrain (un autre terrain avait été développé puis enlevé pour des raisons esthétiques).
- Un autre mode de jeu qui ne se base pas sur des vagues mais sur la progression du joueur dans le niveau (abandonné au départ en raison de mon manque d'expérience pour la création de sprites).
- Ajout de nouvelles musiques et effets sonores.
- Des options dans le menu principal pour le volume, les touches, ...


J'ai travaillé seul sur ce projet, mais j'ai utilisé un script de Sebastian Lague (https://github.com/SebLague) pour la vision du joueur et des zombies (série "Field of view visualisation") que j'ai modifié pour mon projet (ajout d'une liste des cibles sans collision et modification pour fonctionnement dans un environnement 2D).
Les musiques et effets sonores proviennent d'une part de SoundEffectFactory (https://www.youtube.com/channel/UCYIxR_86Ck0sCL26eVuumvQ) pour les musiques et de Stefan Persson (https://www.imphenzia.com/) avec le paquet Unity "Universal Sound FX" pour les différents effets sonores.
J'ai utilisé A* pathfinding pour l'IA.

Quelques sprites sont disponibles à l'adresse suivante : https://www.deviantart.com/corentinpb/gallery/67395694/Zombie-Defense.

Si vous voulez plus d'informations au sujet d'un point précis, donner un feedback ou autre, n'hésitez pas à me contacter à l'adresse : corentin.pacaudboehm@gmail.com.